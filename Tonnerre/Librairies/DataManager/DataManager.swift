//
//  DataManager.swift
//  Tonnerre
//
//  Created by Loris Mazloum on 10/5/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import Foundation
import CoreLocation

protocol DataManagerDelegate {
    func didFetchDataFromStore(_ data: [TimeStamp])
}

class DataManager {
    
    static let shared = DataManager()
    var delegate: DataManagerDelegate?
    let api = ICAPIManager.api
    
    private init() {
        api.delegate = self
    }
    
    func fetchTimestamps() {
        if let fetchedTimestamps = TimeStamp.fetch() as? [TimeStamp] {
            self.delegate?.didFetchDataFromStore(fetchedTimestamps)
        }
    }
    
    func updateStore(with location: CLLocation) {
//        let location: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: CLLocationDegrees(48.856613), longitude: 2.352222)
    
        api.fetchForecast(for: location)
    }
}

extension DataManager: ICAPIManagerDelegate {
    func didReceiveForecast() {
        fetchTimestamps()
    }
}
