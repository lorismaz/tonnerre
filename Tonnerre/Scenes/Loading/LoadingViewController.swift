//
//  LoadingViewController.swift
//  Tonnerre
//
//  Created by Loris Mazloum on 10/4/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import UIKit

class LoadingViewController: UIViewController {

    //MARK: Properties and Outlets
    @IBOutlet weak var logoImageView: UIImageView!
    
    //MARK: App Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logoImageView.alpha = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateLogo()
    }
    
    func animateLogo() {
        UIView.animate(withDuration: 1.5, animations: {
            self.logoImageView.alpha = 1.0
        }) { (complete) in
            self.showDayList()
        }
    }
    
    func showDayList() {
        self.performSegue(withIdentifier: "showDayList", sender: self)
    }

}
