//
//  NSManagedObject+Helper.swift
//  Mazlabs
//
//  Created by Loris Mazloum on 6/1/17.
//  Copyright © 2017 MazLabs. All rights reserved.
//

import UIKit
import CoreData

extension NSManagedObject {
    
    /**
     Get current context from app delegate
     
     ### Usage Example: ###
     ````
     let context = getContext()
     ````
     
     - Returns: Current *context* from app delegates
     
     - Version: 0.1
     
     - Author: Loris Mazloum
     */
    private class func getContext() -> NSManagedObjectContext {
        let appDelegate: AppDelegate
        if Thread.current.isMainThread {
            appDelegate = UIApplication.shared.delegate as! AppDelegate
        } else {
            appDelegate = DispatchQueue.main.sync {
                return UIApplication.shared.delegate as! AppDelegate
            }
        }
        return appDelegate.persistentContainer.viewContext
    }
    
    
    /**
     Store NSManagedObject in database
     
     ### Usage Example: ###
     ````
     Object.save()
     ````
     
     - Version: 0.1
     
     - Author: Loris Mazloum
     */
    
    public static func store(){
        let context = getContext()
        
        do {
            try context.save()
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
    /**
     Create a new instance of the current NSManagedObject
     
     ### Usage Example: ###
     ````
     let object = Object.newInstance()
     object.setValue("12345", forKey: "uuid")
     ````
     - Returns: An instance of NSManagedObject
     
     - Version: 0.1
     
     - Author: Loris Mazloum
     */
    public static func newInstance() -> NSManagedObject{
        let context = getContext()
        let objectString = String(describing: self)
        guard let entity =  NSEntityDescription.entity(forEntityName: objectString, in:context) else { fatalError("Could not create a new instance for the entity \(objectString)")}
        return NSManagedObject(entity: entity, insertInto:context)
    }
    
    /**
     Deletes the current NSManagedObject's instance
     
     ### Usage Example: ###
     ````
     ````
     
     - Version: 0.1
     
     - Author: Loris Mazloum
     */
    public func delete() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        context.delete(self)
    }
    
    /**
     Fetch all the data from the db, returning an array of current NSManagedObject
     
     ### Usage Example: ###
     ````
     ````
     
     - Returns: An array of NSManagedObject
     - Parameter sortDescriptor: NSSortDescriptor with sort parameters
     - Parameter predicate: NSPredicate with predicates parameters
     
     - Version: 0.1
     
     - Author: Loris Mazloum
     */
    public static func fetch(sortDescriptor: NSSortDescriptor? = nil, predicate: NSPredicate? = nil) -> [NSManagedObject] {
        let context = getContext()
        let entityName = String(describing: self)
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        if let sortDescriptor = sortDescriptor {
            fetchRequest.sortDescriptors = [sortDescriptor]
        }
        
        if let predicate = predicate {
            fetchRequest.predicate = predicate
        }
        
        do {
            guard let fetchResult = try context.fetch(fetchRequest) as? [NSManagedObject] else { return [] }
            return fetchResult
        } catch {
            fatalError(error.localizedDescription)
        }
        
        return []
    }
    
}

