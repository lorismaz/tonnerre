//
//  DetailsViewModelTest.swift
//  TonnerreTests
//
//  Created by Loris Mazloum on 10/5/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import XCTest
@testable import Tonnerre

class DetailsViewModelTest: XCTestCase {

    override func setUp() {
    }

    override func tearDown() {
    }

    func testTimestampTemperature() {
        let time1 = TimeStamp(dateTime: Date() as NSDate, temperature: 22.3)
        
        let viewModel = DetailsViewModel(timestamp: time1)
        
        XCTAssert(viewModel.temperatureString == time1.temperatureString)
        
    }

}
