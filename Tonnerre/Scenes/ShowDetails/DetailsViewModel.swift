//
//  DetailsViewModel.swift
//  Tonnerre
//
//  Created by Loris Mazloum on 10/4/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import UIKit

class DetailsViewModel {
    private var timestamps: [TimeStamp] = []
    var day: Date
    
    init(with day: Date) {
        self.day = day
        
        let cal = Calendar(identifier: .gregorian)
        let dayStart: Date = cal.startOfDay(for: day)
        let dayEnd: Date = cal.date(byAdding: .day, value: 1, to: dayStart)!
        let predicate = NSPredicate(format: "dateTime >= %@ && dateTime <= %@", argumentArray: [dayStart,dayEnd])
        if let timestamps = TimeStamp.fetch(predicate: predicate) as? [TimeStamp] {
            self.timestamps = timestamps
        }
    }
    
    var title: String {
        return day.toMonthDayString
    }
    
    var numberOfRowsInSection: Int {
        return timestamps.count
    }
    
    func cell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell", for: indexPath) as? DetailsCell else { return UITableViewCell() }
        
        let sortedTimestamps = timestamps.sorted(by: { (left, right) -> Bool in
            guard let leftDate = left.dateTime as Date?, let rightDate = right.dateTime as Date? else { return false }
            return leftDate.compare(rightDate) == .orderedAscending
        })
        
        let currentTimestamp = sortedTimestamps[indexPath.row]
        
        cell.hourLabel.text = currentTimestamp.hourString
        cell.tempLabel.text = currentTimestamp.temperatureString
        cell.humidityLabel.text = currentTimestamp.humidityString
        
        return cell
    }
    
    func heightForCell(tableView: UITableView, indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
}
