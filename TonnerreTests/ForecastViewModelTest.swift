//
//  ForecastViewModelTest.swift
//  TonnerreTests
//
//  Created by Loris Mazloum on 10/5/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import XCTest
@testable import Tonnerre

class ForecastViewModelTest: XCTestCase {

    override func setUp() {
    }

    override func tearDown() {
    }
    
    func testTimestampCount_Empty() {
        let data: [TimeStamp] = []
        let viewModel = ForecastViewModel(with: data)
        
        XCTAssert(viewModel.forecast.count == 0)
        XCTAssert(viewModel.forecast.isEmpty)
    }
    
    func testTimestampCount_Many() {
        let time1 = TimeStamp(dateTime: Date() as NSDate, temperature: 22.3)
        let time2 = TimeStamp(dateTime: Date() as NSDate, temperature: 23.9)
        
        let data: [TimeStamp] = [time1, time2]
        let viewModel = ForecastViewModel(with:data)
        
        XCTAssert(viewModel.forecast.count == 2)
    }


}
