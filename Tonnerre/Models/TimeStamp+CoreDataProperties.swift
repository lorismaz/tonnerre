//
//  TimeStamp+CoreDataProperties.swift
//  Tonnerre
//
//  Created by Loris Mazloum on 10/5/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//
//

import Foundation
import CoreData


extension TimeStamp {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TimeStamp> {
        return NSFetchRequest<TimeStamp>(entityName: "TimeStamp")
    }

    @NSManaged public var dateTime: NSDate?
    @NSManaged public var temperature: Double
    @NSManaged public var humidity: Double
    
    convenience init(dateTime: NSDate?, temperature: Double) {
        self.init()
        self.dateTime = dateTime
        self.temperature = temperature
    }
    
    var temperatureString: String {
        let temp = String(format: "%.0f", temperature)
        return "\(temp)°C"
    }
    
    var humidityString: String {
        let humi = String(format: "%.0f", humidity)
        return "\(humi)%"
    }
    
    var hourString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        guard let date = self.dateTime as Date? else { return "" }
        return dateFormatter.string(from: date)
    }
    
    var dayString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM d"
        guard let date = self.dateTime as Date? else { return "" }
        return dateFormatter.string(from: date)
    }
}
