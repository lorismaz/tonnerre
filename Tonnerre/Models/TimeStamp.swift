//
//  TimeStamp
//  Tonnerre
//
//  Created by Loris Mazloum on 10/4/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import Foundation

class TimeStamp: Decodable {
    let dateTime: Date
    let temperature: Double
    
    init(dateTime: Date, temperature: Double) {
        self.dateTime = dateTime
        self.temperature = temperature
    }
    
    init(json: [String: Any], date: String) {
        guard let dateTime = date.toDate else { fatalError("can't parse json dateTime") }
        self.dateTime = dateTime
        
        guard let temperatureDict = json["temperature"] as? [String: Any] else { fatalError("can't parse json temperature object") }
        guard var temperature = temperatureDict["2m"] as? Double else { fatalError("can't parse json temperature details") }
        temperature -= 273.15
        self.temperature = temperature
    }
    
    var temperatureString: String {
        let temp = String(format: "%.0f", temperature)
        return "\(temp)°C"
    }
    
    var hourString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        return dateFormatter.string(from: self.dateTime)
    }
    
    var dayString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM d"
        
        return dateFormatter.string(from: self.dateTime)
    }
}




