//
//  ForecastViewModel.swift
//  Tonnerre
//
//  Created by Loris Mazloum on 10/4/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import UIKit
import CoreLocation

protocol ForecastViewModelDelegate: class {
    func didLoadForecast()
}

class ForecastViewModel {
    var timestampList = [TimeStamp]()
    var delegate: ForecastViewModelDelegate?
    let dataStore = DataManager.shared
    
    init() {
        dataStore.delegate = self
        dataStore.fetchTimestamps()
    }
    
    var forecast: [Date] {
        let cal = Calendar.init(identifier: .gregorian)
        
        let predicate: (TimeStamp) -> Date = {  cal.startOfDay(for: $0.dateTime! as Date) }
        let groupedForecast = Dictionary(grouping: timestampList, by: predicate)
        let forecast = Array(groupedForecast.keys).sorted { (left, right) -> Bool in
            return left.compare(right ) == .orderedAscending
        }
    
        return forecast
    }
    
    var numberOfRowsInSection: Int {
        return forecast.count == 0 ? 1 : forecast.count
    }
    
    func cell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
    
        if forecast.count == 0 {
            return tableView.dequeueReusableCell(withIdentifier: "emptyDataset")!
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "forecastCell", for: indexPath) as? ForecastCell else { return UITableViewCell() }
        
        let currentDay = forecast[indexPath.row]
        
        let dayTimestamps = timestampList.filter { (timestamp) -> Bool in
            let cal = Calendar.init(identifier: .gregorian)
            return cal.startOfDay(for: timestamp.dateTime! as Date) == cal.startOfDay(for: currentDay)
        }
        
        let maxTemp = dayTimestamps.max(by: { (left, right) -> Bool in
            left.temperature < right.temperature
        })
        
        let lowTemp = dayTimestamps.min(by: { (left, right) -> Bool in
            left.temperature < right.temperature
        })
        
        cell.timeLabel.text = currentDay.toMonthDayString
        
        cell.lowTempLabel.text = lowTemp?.temperatureString
        cell.highTempLabel.text = maxTemp?.temperatureString
        
        return cell
    }
    
    func heightForCell(tableView: UITableView, indexPath: IndexPath) -> CGFloat {
        if forecast.count == 0 {
            return tableView.frame.height
        }
        
        return 88
    }
    
    func selectedTimestamp(tableView: UITableView, indexPath: IndexPath) -> Date {
        return forecast[indexPath.row]
    }
    
    func updateForecast(for location: CLLocation) {
        dataStore.updateStore(with: location)
    }
}

extension ForecastViewModel: DataManagerDelegate {    
    func didFetchDataFromStore(_ data: [TimeStamp]) {
        self.timestampList = data
        self.delegate?.didLoadForecast()
    }
}
