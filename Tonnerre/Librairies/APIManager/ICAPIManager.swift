//
//  ICAPIManager.swift
//  Tonnerre
//
//  Created by Loris Mazloum on 10/4/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData

protocol ICAPIManagerDelegate {
    func didReceiveForecast()
}

enum ICAPIServices {
    case forecast(location: CLLocation)
}

enum ICAPIParameters: String {
    case auth = "_auth"
    case key = "_c"
    case location = "_ll"
}

extension ICAPIServices {
    public var ressource : URL {
        switch self {
        case .forecast(let location):
            guard let url = buildURL(with: location).url else { fatalError("incorrect URL string") }
            return url
        }
    }
    
    private func buildURL(with location: CLLocation) -> URLComponents {
        
        let locationString = location.coordinate.latitude.description+","+location.coordinate.longitude.description
        let locationItem = URLQueryItem(name: ICAPIParameters.location.rawValue, value: locationString)
        let auth = URLQueryItem(name: ICAPIParameters.auth.rawValue, value: ICAPIManager.auth)
        let key = URLQueryItem(name: ICAPIParameters.key.rawValue, value: ICAPIManager.key)
        
        let queryComponents = [locationItem,auth, key]
        
        var urlComponents = URLComponents(string: ICAPIManager.baseURL)
        urlComponents?.queryItems = queryComponents
        guard let builtURL = urlComponents else { fatalError("Can't build a url from these coordinates") }
        print(builtURL.description)
        return builtURL
    }
}

class ICAPIManager {
    
    static let api = ICAPIManager()
    
    static let baseURL: String = "http://www.infoclimat.fr/public-api/gfs/json"
    static let auth: String = "U0kFEgZ4UHJSf1RjVSNSe1c%2FBjMMegMkAHxVNlo%2FAn8IYwRlUjJXMQNtAH0PIAs9WHVSMQw3VGQCaQpyAXMEZVM5BWkGbVA3Uj1UMVV6UnlXeQZnDCwDJABqVTFaKQJjCHUEYVIxVzQDcgBjDzgLOlh0Ui0MMlRpAmIKZQFqBGNTOAVhBm1QNVIiVClVYFI0VzYGYwxmAzMAYlU1WjQCNAhvBDJSZVc9A3IAag83Cz9YaFI3DDpUbQJnCnIBcwQeU0MFfAYlUHBSaFRwVXhSM1c6BjI%3D".removingPercentEncoding!
    static let key: String = "534c99d06492de0b532e54b6b860febf"
    
    var delegate: ICAPIManagerDelegate?
    let sharedSession = URLSession.shared
    
    private init() {
    }
    
    func fetchForecast(for location: CLLocation) {
        
        var request = URLRequest(url: ICAPIServices.forecast(location: location).ressource)
        request.httpMethod = "GET"
        
        let completionHandler: (Data?, URLResponse?, Error?) -> Void = { data, response, error in
            guard error == nil else { return }
            guard let responseData = data else { return }
            
            let _ = self.getForecast(from: responseData, completion: {
                if let existingDelegate = self.delegate {
                    existingDelegate.didReceiveForecast()
                }
            })
        }
        
        
        sharedSession.dataTask(with: request, completionHandler: completionHandler).resume()
    }
    
    private func getForecast(from data: Data, completion: @escaping () -> ()) {
        let datesDictionnary = extractDatesDictionnary(with: data)
        _ = buildForecast(from: datesDictionnary, completion: {
            completion()
        })
    }
    
    private func extractDatesDictionnary(with data: Data) -> [String: Any] {
        var forecastDictionnary: [String: Any] = [:]
        
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data, options:
                JSONSerialization.ReadingOptions.allowFragments)
            guard let jsonDictionnary = jsonObject as? [String:Any],
                let requestState = jsonDictionnary["request_state"] as? Int,
                requestState == 200 else { return [:] }
            
            for (key, value) in jsonObject as! [String: Any] {
                
                if key.toDate != nil {
                    forecastDictionnary[key] = value
                }
            }
        } catch let error as NSError {
            print("Could not save. \(error)")
        }
        
        return forecastDictionnary
    }
    
    private func buildForecast(from dictionnary: [String: Any], completion: @escaping () -> ()) {
        
            let dateFormatter = DateFormatter()
            
            for object in dictionnary {
                guard let details = object.value as? [String: Any] else {fatalError()}
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                guard let date = dateFormatter.date(from: object.key) else {fatalError()}
                
                guard let temperatureDict = details["temperature"] as? [String: Any] else { fatalError("can't parse json temperature object") }
                guard var temperature = temperatureDict["2m"] as? Double else { fatalError("can't parse json temperature details") }
                temperature -= 273.15
                
                guard let humidityDict = details["humidite"] as? [String: Any] else { fatalError("can't parse json humidity object") }
                guard let humidity = humidityDict["2m"] as? Double else { fatalError("can't parse json humidity details") }
                
                //Store each timsestamp in core data
                self.createEntity(date: date, temperature: temperature, humidity: humidity, completion: {
                    self.saveStore(completion: {
                        completion()
                    })
                })
            }

    }
    
    
    private func clearStore(completion: @escaping () -> ()) {
        DispatchQueue.main.async {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            let managedContext = appDelegate.persistentContainer.viewContext
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TimeStamp")
            let deleteReqest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            
            do {
                try managedContext.execute(deleteReqest)
            } catch {
                print(error)
            }
        }
    }
    
    private func saveStore(completion: @escaping () -> ()) {
        DispatchQueue.main.async {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            let managedContext = appDelegate.persistentContainer.viewContext
            do {
                try managedContext.save()
                completion()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    private func createEntity(date: Date, temperature: Double, humidity: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.async {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
            let managedContext = appDelegate.persistentContainer.viewContext
            let timestampEntity = NSEntityDescription.entity(forEntityName: "TimeStamp", in: managedContext)!
            
            guard let timestamp = NSManagedObject(entity: timestampEntity, insertInto: managedContext) as? TimeStamp else { return }
            
            timestamp.setValue(date, forKey: "dateTime")
            timestamp.setValue(temperature, forKey: "temperature")
            timestamp.setValue(humidity, forKey: "humidity")
            
            do {
                try managedContext.save()
                completion()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
}
