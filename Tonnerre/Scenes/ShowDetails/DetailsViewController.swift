//
//  DetailsViewController.swift
//  Tonnerre
//
//  Created by Loris Mazloum on 10/4/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    
    //MARK: - Properties
    var detailsViewModel: DetailsViewModel? = nil
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var detailsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTopView()
        setupTable()
    }
    
    func setupTopView() {
        guard let detailsViewModel = detailsViewModel else { return }
        self.title = detailsViewModel.title
    }

}

extension DetailsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func setupTable() {
        detailsTableView.dataSource = self
        detailsTableView.delegate = self
        detailsTableView.tableFooterView = UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let viewModel = detailsViewModel else { fatalError("Viewmodel incorrect") }
        return viewModel.numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let viewModel = detailsViewModel else { fatalError("Viewmodel incorrect") }
        return viewModel.cell(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let viewModel = detailsViewModel else { fatalError("Viewmodel incorrect") }
        return viewModel.heightForCell(tableView: tableView, indexPath: indexPath)
    }
    
    
}
