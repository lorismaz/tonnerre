//
//  ForecastViewController.swift
//  Tonnerre
//
//  Created by Loris Mazloum on 10/4/18.
//  Copyright © 2018 Loris Mazloum. All rights reserved.
//

import UIKit
import CoreLocation

class ForecastViewController: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var forecastTableView: UITableView!
    
    //MARK: - Properties
    let forecastViewModel = ForecastViewModel()
    let locationManager = CLLocationManager()
    
    //MARK: - App Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        forecastViewModel.delegate = self
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    }

}

extension ForecastViewController: UITableViewDataSource {
    
    func setupTable() {
        forecastTableView.dataSource = self
        forecastTableView.delegate = self
        forecastTableView.tableFooterView = UIView(frame: .zero)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecastViewModel.numberOfRowsInSection
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return forecastViewModel.cell(tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return forecastViewModel.heightForCell(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return forecastViewModel.heightForCell(tableView: tableView, indexPath: indexPath)
    }
    
}

extension ForecastViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if !forecastViewModel.forecast.isEmpty {
            let currentTimestamp = forecastViewModel.selectedTimestamp(tableView: tableView, indexPath: indexPath)
            performSegue(withIdentifier: "showDetails", sender: currentTimestamp)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? DetailsViewController else { fatalError("Can't get a DetailsViewController")}
        guard let day = sender as? Date else { fatalError("Sender is not a date")}
        
        destination.detailsViewModel = DetailsViewModel(with: day)
    }
}

extension ForecastViewController: ForecastViewModelDelegate {
    
    func didLoadForecast() {
        DispatchQueue.main.async {
            self.forecastTableView.reloadData()
        }
    }
}

extension ForecastViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        forecastViewModel.updateForecast(for: location)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error while getting location: \(error.localizedDescription) ")
    }
}
